import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    monthList: [
      {
        abbreviation: "Jan",
        name: "January",
        number: 1,
      },
      {
        abbreviation: "Feb",
        name: "February",
        number: 2,
      },
      {
        abbreviation: "Mar",
        name: "March",
        number: 3,
      },
      {
        abbreviation: "Apr",
        name: "April",
        number: 4,
      },
      {
        abbreviation: "May",
        name: "May",
        number: 5,
      },
      {
        abbreviation: "Jun",
        name: "June",
        number: 6,
      },
      {
        abbreviation: "Jul",
        name: "July",
        number: 7,
      },
      {
        abbreviation: "Aug",
        name: "August",
        number: 8,
      },
      {
        abbreviation: "Sep",
        name: "September",
        number: 9,
      },
      {
        abbreviation: "Oct",
        name: "October",
        number: 10,
      },
      {
        abbreviation: "Nov",
        name: "November",
        number: 11,
      },
      {
        abbreviation: "Dec",
        name: "December",
        number: 12,
      },
    ],
  },
  mutations: {},
  actions: {},
  getters: {
    getMonthList(state) {
      return state.monthList;
    },
  },
});
